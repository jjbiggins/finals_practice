import java.util.Random;

public class CollisionsTest {

	public void runTest(int num_keys) {
		// use the typical load factor of 0.75
		// That is, make 0.75*capacity = number of elements
		CS2230HashMap<ThanksgivingString,Integer> m = new CS2230HashMap<>((int)(num_keys/0.75));
		int collisions = 0;

		Random r = new Random();
		for (int i=0; i<num_keys; i++) {
			// generate a random string
			byte[] str = new byte[6];
			r.nextBytes(str);
			ThanksgivingString key = new ThanksgivingString(new String(str));
			Integer value = r.nextInt();

			// check the hashCode/equals property holds
			ThanksgivingString copy = new ThanksgivingString(new String(str));
			if (copy.equals(key)) {
					if (copy.hashCode()!=key.hashCode()) {
						throw new AssertionError("your hashCode() violates q.equals(p) implies q.hashCode()==p.hashCode()");
					}
			}

			// count collisions
			try {
				m.put(key, value);
			} catch (UnsupportedOperationException e) {
				collisions++;
			}
		}	
		System.out.println("collision % = "+(double)collisions/num_keys*100);
	}


    public static void main(String args[]) throws Exception{
		CollisionsTest test = new CollisionsTest();
		test.runTest(10000);

    }
    
    static class ThanksgivingString {
      public String s;
      public ThanksgivingString(String str){
          s = str;
      }
      @Override
      public int hashCode(){
            // change this implementation
			return 1;
      }

		@Override
		public boolean equals(Object obj) {
				if (obj instanceof ThanksgivingString)
					return s.equals(((ThanksgivingString)obj).s);
				else
						return false;
		}
	}
}
