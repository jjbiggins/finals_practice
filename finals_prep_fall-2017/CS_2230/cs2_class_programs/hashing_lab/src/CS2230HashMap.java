public class CS2230HashMap<K,V> implements CS2230Map<K,V> {

		@Override
		public V get(K key) {
				return null;
		}

		@Override
		public V put(K key, V value) {
				// key.hashCode() modulo tableSize
				int i = Math.floorMod(key.hashCode(), tableSize);
				// Case 1:_______________________
				if(table[i] == null){
					table[i] = new HashEntry(key, value);
					return null;
				// Case 2:_______________________
				} else if (table[i].key.equals(key)) {
					V temp = table[i].value;
					table[i] = new HashEntry(key, value);
					return temp;
				// Case 3:_______________________
				} else {
					// note that a complete hash map implementation would
					// handle collisions (usually by chaining or probing)
					throw new UnsupportedOperationException("Does not handle collisions (index="+i+")");        
				}
		}

    private HashEntry<K,V>[] table;
    private int tableSize;
    
    public CS2230HashMap(int tableSize){
        table = new HashEntry[tableSize];
        this.tableSize = tableSize;
    }
    
    private void printTable(){
		int j=0;
        for(HashEntry<K,V> i : table){
			System.out.print(" ["+j+"]");
            if(i != null)
                System.out.print(i.key + ":" + i.value + " " );
            else
                System.out.print("-- ");
			j++;
        }
        System.out.println();
    }
    
    public static void main(String args[]) {
        CS2230HashMap<String,String> map = new CS2230HashMap(10);

        map.printTable();        
        map.put("turkey", "1");
        map.printTable();        
        map.put("family", "7");
        map.printTable();        

		if (map.get("turkey") == null 
				|| !map.get("turkey").equals(1)) throw new AssertionError();
		if (map.get("family") == null 
				|| !map.get("family").equals(1)) throw new AssertionError();
		if (map.get("cranberry")!=null) throw new AssertionError();

        map.put("pumpkin pie", "8");
        map.printTable();        
        map.put("apple pie", "3");
        map.printTable();        
    }

    
    private class HashEntry<K,V> {
      public final K key;
      public final V value;

      HashEntry(K key, V value) {
            this.key = key;
            this.value = value;
      }     
    }

}
