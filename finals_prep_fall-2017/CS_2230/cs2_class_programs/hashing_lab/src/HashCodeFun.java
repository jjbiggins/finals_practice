import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class HashCodeFun {
	public static void main(String[] args) {
        Map<TurkeyString, Integer> map = new HashMap();
        System.out.println(map);     
        map.put(new TurkeyString("turkey"), 1);
        map.put(new TurkeyString("stuffing"), 2);
        map.put(new TurkeyString("sweet potato"), 0);
        map.put(new TurkeyString("family"), 7);
        System.out.println(map);        

        map.put(new TurkeyString("sweet potato"), 6);
        System.out.println(map);        

		Integer numTurkeys = map.get(new TurkeyString("turkey"));
		System.out.println(numTurkeys);
	}	
	
	private static class TurkeyString {
		private final String s;
		private final static Random rand = new Random(1);
		@Override
		public String toString() {
				return s;
		}
		public TurkeyString(String s) {
				this.s = s;
		}
		@Override
		public int hashCode() {
				return rand.nextInt();
		}
		@Override
		public boolean equals(Object obj) {
				if (obj instanceof TurkeyString)
					return s.equals(((TurkeyString)obj).s);
				else
						return false;
		}
	}
}
