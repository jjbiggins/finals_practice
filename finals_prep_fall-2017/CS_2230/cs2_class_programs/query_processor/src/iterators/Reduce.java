package iterators;


import java.util.Iterator;

// Iterator that returns a single element that is the result of
// combining all the input elements
public class Reduce<InT,OutT> implements Iterator<OutT> {
                
                private ReduceFunction<InT, OutT> f;
                private Iterator<InT> input;
                private OutT sum;
                private int count;
                private int currentElement;
                
		public Reduce(ReduceFunction<InT,OutT> f, Iterator<InT> input) {
                    this.input = input;
                    this.f = f;
                    count = 0;
                    currentElement = 0;
		}

		@Override
		public boolean hasNext() {
                    if(count == -1){
                        return false;
                    }
                    else if(input.hasNext()){
                        currentElement++;
                        return true;
                    }
                    else {
                        count++;
                        return true;
                    }
		}

		@Override
		public OutT next() {
                    if(count > currentElement){
                        count = -1;
                        sum = f.initialValue();
                        return sum;
                    }
                    else{
                        count = -1;
                        sum = f.combine(f.initialValue(), input.next());
                        return sum;
                    }
		}
}
