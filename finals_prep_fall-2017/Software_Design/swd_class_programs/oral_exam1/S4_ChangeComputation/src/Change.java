public class Change {

    private double changeAmount;
    private  CashRegister cashRegister;
    private int dollars20;
    private int dollars10;
    private int dollars5;
    private int dollars1;
    private int quarters;
    private int dimes;
    private int nickels;
    private int pennies;


    public Change(double itemPrice, double customerPaid, CashRegister availableBills){
        changeAmount = customerPaid - itemPrice;
        cashRegister = availableBills;
    }

   private void roundToTwoDecimals(double changeAmount){
        changeAmount = changeAmount * 100;
        changeAmount = (int) changeAmount;
        this.changeAmount = changeAmount / 100;
   }


    public double getChange() {
       roundToTwoDecimals(changeAmount);
       return changeAmount;
    }

    public void billsAndCoinsToReturn(double changeAmount){
        if(this.changeAmount >= 20.0){
            if(cashRegister.getNumberOfTwentiesInRegister() >= (int) (this.changeAmount / 20)) {
                this.setDollars20((int) (this.changeAmount / 20));
                cashRegister.setNumberOfTwentiesInRegister(cashRegister.getNumberOfTwentiesInRegister() - this.getDollars20());
                int amountReturnedInTwenties = this.getDollars20() * 20;
                this.changeAmount = this.changeAmount - amountReturnedInTwenties;
            }
            else{
                this.setDollars20(cashRegister.getNumberOfTwentiesInRegister());
                cashRegister.setNumberOfTwentiesInRegister(0);
            }
        }
        if (this.changeAmount >= 10.0){
            if(cashRegister.getNumberOfTensInRegister() >= (int) (this.changeAmount / 10)) {
                this.setDollars10((int) (this.changeAmount / 10));
                cashRegister.setNumberOfTensInRegister(cashRegister.getNumberOfTensInRegister() - this.getDollars10());
                int amountReturnedInTens = this.getDollars10() * 10;
                this.changeAmount = this.changeAmount - amountReturnedInTens;
            }
            else{
                this.setDollars10(cashRegister.getNumberOfTensInRegister());
                cashRegister.setNumberOfTensInRegister(0);
            }
        }
        if (this.changeAmount >= 5){
            if(cashRegister.getNumberOfFivesInRegister() >= (int) (this.changeAmount / 5)) {
                this.setDollars5((int) (this.changeAmount / 5));
                cashRegister.setNumberOfFivesInRegister(cashRegister.getNumberOfFivesInRegister() - this.getDollars5());
                int amountReturnedInFives = this.getDollars5() * 5;
                this.changeAmount = this.changeAmount - amountReturnedInFives;
            }
            else{
                this.setDollars5(cashRegister.getNumberOfFivesInRegister());
                cashRegister.setNumberOfFivesInRegister(0);
            }
        }
        if (this.changeAmount >= 1){
            if(cashRegister.getNumberOfOnesInRegister() >= (int) changeAmount) {
                this.setDollars1((int) (this.changeAmount / 1));
                cashRegister.setNumberOfOnesInRegister(cashRegister.getNumberOfOnesInRegister() - this.getDollars1());
                int amountReturnedInOnes = this.getDollars1();
                this.changeAmount = this.changeAmount - amountReturnedInOnes;
            }
            else{
                this.setDollars1(cashRegister.getNumberOfOnesInRegister());
                cashRegister.setNumberOfOnesInRegister(0);
            }
        }
        if (this.changeAmount >= 0.25){
            if(cashRegister.getNumberOfQuartersInRegister() >= (int) (changeAmount / 0.25)){
                this.setQuarters((int) (this.changeAmount / 0.25));
                cashRegister.setNumberOfQuartersInRegister(cashRegister.getNumberOfQuartersInRegister() - this.getQuarters());
                double amountReturnedInQuarters = this.getQuarters() * 0.25;
                this.changeAmount = this.changeAmount - amountReturnedInQuarters;
            }
            else{
                this.setQuarters(cashRegister.getNumberOfQuartersInRegister());
                cashRegister.setNumberOfQuartersInRegister(0);
            }
        }
        if (this.changeAmount >= 0.10){
            if(cashRegister.getNumberOfDimesInRegister() >= (int) (changeAmount / 0.10)) {
                this.setDimes((int) (this.changeAmount / 0.10));
                cashRegister.setNumberOfDimesInRegister(cashRegister.getNumberOfDimesInRegister() - this.getDimes());
                double amountReturnedInDimes = this.getDimes() * 0.10;
                this.changeAmount = this.changeAmount - amountReturnedInDimes;
            }
            else{
                this.setDimes(cashRegister.getNumberOfDimesInRegister());
                cashRegister.setNumberOfDimesInRegister(0);
            }
        }
        if (this.changeAmount >= 0.05){
            if(cashRegister.getNumberOfNickelsInRegister() >= (int) (changeAmount / 0.05)) {
                this.setNickels((int) (this.changeAmount / 0.05));
                cashRegister.setNumberOfNickelsInRegister(cashRegister.getNumberOfNickelsInRegister() - this.getNickels());
                double amountReturnedInNickels = this.getNickels() * 0.05;
                this.changeAmount = this.changeAmount - amountReturnedInNickels;
            }
            else{
                this.setNickels(cashRegister.getNumberOfNickelsInRegister());
                cashRegister.setNumberOfNickelsInRegister(0);
            }
        }
        if (this.changeAmount >= 0.01){
            if(cashRegister.getNumberOfPenniesInRegister() >= (int) (changeAmount / 0.01)) {
                this.setPennies((int) ((this.changeAmount + 0.01) / 0.01));
                cashRegister.setNumberOfPenniesInRegister(cashRegister.getNumberOfPenniesInRegister() - this.getPennies());
                double amountReturnedInPennies = this.getPennies() * 0.01;
                this.changeAmount = this.changeAmount - amountReturnedInPennies;
            }
            else{
                this.setPennies(cashRegister.getNumberOfPenniesInRegister());
                cashRegister.setNumberOfPenniesInRegister(0);
            }
        }
    }

    public int getDollars20() {
        return dollars20;
    }

    public void setDollars20(int dollars20) {
        this.dollars20 = dollars20;
    }


    public int getDollars10() {
        return dollars10;
    }

    public void setDollars10(int dollars10) {
        this.dollars10 = dollars10;
    }

    public int getDollars5() {
        return dollars5;
    }

    public void setDollars5(int dollars5) {
        this.dollars5 = dollars5;
    }

    public int getDollars1() {
        return dollars1;
    }

    public void setDollars1(int dollars1) {
        this.dollars1 = dollars1;
    }

    public int getQuarters() {
        return quarters;
    }

    public void setQuarters(int quarters) {
        this.quarters = quarters;
    }

    public int getDimes() {
        return dimes;
    }

    public void setDimes(int dimes) {
        this.dimes = dimes;
    }

    public int getNickels() {
        return nickels;
    }

    public void setNickels(int nickels) {
        this.nickels = nickels;
    }

    public int getPennies() {
        return pennies;
    }

    public void setPennies(int pennies) {
        this.pennies = pennies;
    }
}
